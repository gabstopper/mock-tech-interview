import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { FormDataContext } from "../context/FormDataContext.jsx";

function MedicalHistory() {
  const navigate = useNavigate();
  const { formData, setFormData } = useContext(FormDataContext);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setTimeout(() => {
    console.log(formData); // Log the final formData before navigation
    navigate("/family-relations");
    }, 1000);    
  };

  return (
    <div className="bg-gray-100 p-5 h-dvh">
      <h1 className="text-center text-2xl font-semibold mb-2">
        Medical History
      </h1>
      <section className="flex justify-center w-full py-3">
        <form onSubmit={handleSubmit}>
          <fieldset>
            <legend>Do you drink alcohol?</legend>
            <div className="flex justify-center py-2">
              <div className="mx-1">
                <input
                  type="radio"
                  name="alcohol"
                  id="alcoholYes"
                  value="Yes"
                  checked={formData.alcohol === "Yes"}
                  onChange={handleInputChange}
                  required
                />
                <label htmlFor="alcoholYes"> Yes</label>
              </div>
              <div className="mx-5">
                <input
                  type="radio"
                  name="alcohol"
                  id="alcoholNo"
                  value="No"
                  checked={formData.alcohol === "No"}
                  onChange={handleInputChange}
                  required
                />
                <label htmlFor="alcoholNo"> No</label>
              </div>
            </div>
            {formData.alcohol === "Yes" && (
              <div className="flex justify-center py-2">
                <label htmlFor="alcoholFrequency">How Often?</label>
                <textarea
                  id="alcoholFrequency"
                  name="alcoholFrequency"
                  placeholder="Occasionally"
                  className="ml-2 p-2 rounded-lg"
                  value={formData.alcoholFrequency || ""}
                  onChange={handleInputChange}
                ></textarea>
              </div>
            )}
          </fieldset>

          <fieldset>
            <legend>Do you smoke?</legend>
            <div className="flex justify-center py-2">
              <div className="mx-1">
                <input
                  type="radio"
                  name="smoke"
                  id="smokeYes"
                  value="Yes"
                  checked={formData.smoke === "Yes"}
                  onChange={handleInputChange}
                  required
                />
                <label htmlFor="smokeYes"> Yes</label>
              </div>
              <div className="mx-5">
                <input
                  type="radio"
                  name="smoke"
                  id="smokeNo"
                  value="No"
                  checked={formData.smoke === "No"}
                  onChange={handleInputChange}
                  required
                />
                <label htmlFor="smokeNo"> No</label>
              </div>
            </div>
            {formData.smoke === "Yes" && (
              <div className="flex justify-center py-2">
                <label htmlFor="smokeFrequency">How Often?</label>
                <textarea
                  id="smokeFrequency"
                  name="smokeFrequency"
                  placeholder="Occasionally"
                  className="ml-2 p-2 rounded-lg"
                  value={formData.smokeFrequency || ""}
                  onChange={handleInputChange}
                ></textarea>
              </div>
            )}
          </fieldset>

          <fieldset>
            <legend>Blood Type</legend>
            <div className="flex justify-center py-2">
              <input
                type="text"
                name="bloodType"
                id="bloodType"
                placeholder="e.g., O+, A-"
                className="p-2 rounded-lg w-full"
                value={formData.bloodType || ""}
                onChange={handleInputChange}
                required
              />
            </div>
          </fieldset>

          <fieldset>
            <legend>Other Previous Conditions</legend>
            <div className="flex justify-center py-2">
              <textarea
                name="previousConditions"
                id="previousConditions"
                placeholder="Describe any previous conditions here..."
                className="p-2 rounded-lg w-full"
                value={formData.previousConditions || ""}
                onChange={handleInputChange}
              ></textarea>
            </div>
          </fieldset>

          <div className="flex justify-center full mt-5 w-full">
            <button
              type="submit"
              className="bg-slate-300 hover:bg-slate-400 hover:text-white p-3 rounded-lg shadow-md font-semibold w-full"
            >
              Next Page
            </button>
          </div>
        </form>
      </section>
    </div>
  );
}

export default MedicalHistory;
