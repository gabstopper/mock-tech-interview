import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { FormDataContext } from "../context/FormDataContext.jsx";

function BasicInfo() {
  const formFields = [
    { label: "Name:", type: "text", name: "name", placeholder: "John Doe" },
    {
      label: "Birthday:",
      type: "date",
      name: "birthday",
    },
    {
      label: "Address:",
      type: "text",
      name: "address",
      placeholder: "221B Baker Street, London, England",
    },
    {
      label: "Email Address:",
      type: "email",
      name: "email",
      placeholder: "abc@gmail.com",
    },
    {
      label: "Phone number:",
      type: "text",
      name: "phoneNumber",
      placeholder: "09123456789",
    },
    {
      label: "Height (cm):",
      type: "number",
      name: "height",
      placeholder: "182",
      min: "1",
    },
    {
      label: "Weight (kg):",
      type: "number",
      name: "weight",
      placeholder: "74",
      min: "1",
    },
  ];

  // Navigator
  const navigate = useNavigate();

  // State to initialize form data
  const { formData, setFormData } = useContext(FormDataContext);

  // Function to handle input changes and set form data
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  // Function to handle form submission
  const handleSubmit = (event) => {
    event.preventDefault();
    setTimeout(() => {
      navigate("/medical-history");
    }, 1000);
  };

  return (
    <div className="bg-gray-100 p-5 h-dvh">
      <h1 className="text-center text-2xl font-semibold mb-2">Basic Info</h1>
      <section className="flex justify-center w-full py-3">
        <form onSubmit={handleSubmit}>
          {formFields.map((field, index) => (
            <div key={index} className="mb-1 p-2 w-full">
              <label>
                {field.label}
                <input
                  type={field.type}
                  name={field.name}
                  placeholder={field.placeholder}
                  value={formData[field.name] || ""}
                  onChange={handleInputChange}
                  className="p-2 rounded-lg mt-2 w-full"
                  required
                  min={field.min}
                />
              </label>
            </div>
          ))}
          <div className="flex justify-center full mt-5 w-full">
            <button
              type="submit"
              className="bg-slate-300 hover:bg-slate-400 hover:text-white p-3 rounded-lg shadow-md font-semibold w-full"
            >
              Next Page
            </button>
          </div>
        </form>
      </section>
    </div>
  );
}

export default BasicInfo;
