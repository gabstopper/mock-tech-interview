import { createBrowserRouter, RouterProvider } from "react-router-dom";
import BasicInfo from "./components/BasicInfo";
import MedicalHistory from "./components/MedicalHistory";
import FamilyRelations from "./components/FamilyRelations";

const router = createBrowserRouter([
  {
    path: "/",
    element: <BasicInfo />,
    errorElement: <div>Not Found</div>,
  },
  {
    path: "/medical-history",
    element: <MedicalHistory />,
  },
  {
    path: "/family-relations",
    element: <FamilyRelations />,
  },
]);

export default function App() {
  return <RouterProvider router={router}/>;
}
